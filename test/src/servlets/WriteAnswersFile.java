package servlets;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import utils.FileHandler;
import utils.Survey;

/**
 * Servlet implementation class WriteAnswersFile
 */
//@WebServlet("/WriteAnswersFile")
public class WriteAnswersFile extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * Default constructor.
	 */
	public WriteAnswersFile() {
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		Survey survey = new Survey();

		survey.setAnswerOne(request.getParameter("answerOne"));
		survey.setAnswerTwo(request.getParameter("answerTwo"));
		survey.setAnswerThree(request.getParameter("answerThree"));
		
		FileHandler writer = new FileHandler();
		writer.writeAnswerFile(survey);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

}
