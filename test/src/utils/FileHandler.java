package utils;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

import json.JSONArray;
import json.JSONException;
import json.JSONObject;

public class FileHandler {

	
	public void writeAnswerFile(Survey survey) {
		String fileName = "/Users/amulyayadav/Work/Company/push-notification-server/example.xml";
		try {
			File file = new File(fileName);
			BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(
					file));
			bufferedWriter.write("<?xml version=\"1.0\" encoding=\"ISO-8859-1\"?>");
			bufferedWriter.newLine();
			bufferedWriter.write("<test>");
			bufferedWriter.newLine();
			bufferedWriter.write("<medium>"+ survey.getAnswerOne()+ "</medium>");
			bufferedWriter.newLine();
			bufferedWriter.write("<numTweets>"+ survey.getAnswerTwo()+ "</numTweets>");
			bufferedWriter.newLine();
			bufferedWriter.write("<content>"+ survey.getAnswerThree()+ "</content>");
			bufferedWriter.newLine();
			bufferedWriter.write("</test>");
			bufferedWriter.newLine();

			bufferedWriter.close();
			// System.out.println("I'm here " + fileName + " " +
			// survey.getAnswerOne() + " " + survey.getAnswerTwo());
		} catch (IOException ioException) {
			System.err.println("Exception: " + ioException.getMessage());
		    ioException.printStackTrace();
		} catch (Exception exception) {
			System.err.println("Exception: " + exception.getMessage());
			exception.printStackTrace();
		}
	}

}
