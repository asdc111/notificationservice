var http = require('http');
var fs = require('fs');


var redis = require("redis");
//var pubsubclient = redis.createClient(config.port, config.host);
var pubsubClient = redis.createClient();////default takes the correct values

pubsubClient.subscribe("redis_channel");


pubsubClient.on("message", handleMessage);

var socketList = [];

function handleMessage(channel, message) {
    // Error handling elided for brevity
    var payload = JSON.parse(message);

	for (i=0;i<socketList.length;i++)
	{
		if (payload.hasOwnProperty(socketList[i].username))
		{
			socketList[i].emit('notification', 'New notification,' + socketList[i].username);
		}
	}
		
}


// Loading the file index.html displayed to the client
var server = http.createServer(function(req, res) {
    fs.readFile('./index.html', 'utf-8', function(error, content) {
        res.writeHead(200, {"Content-Type": "text/html"});
        res.end(content);
    });
});

// Loading socket.io
var io = require('socket.io').listen(server);


io.sockets.on('connection', function (socket, username) {
	
	socketList.push(socket);

    socket.on('little_newbie', function(username) {
        socket.username = username;

	/*for (i=0;i<socketList.length;i++)
    {
        if (socketList[i].username == 'smalling')
        	socket.emit('notification', 'New notification');
    }*/

    });

    // When a "message" is received (click on the button), it's logged in the console
   // socket.on('message', function (message) {
        // The username of the person who clicked is retrieved from the session variables
     //   console.log(socket.username + ' is speaking to me! They\'re saying: ' + message);
    //}); 
});


server.listen(8080);
